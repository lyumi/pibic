from __future__ import division
__author__ = 'daniel'

import sys, os

pathup = os.path.abspath(os.path.join(
    os.path.dirname(__file__+"/../../"), os.path.pardir))
sys.path.insert(0, pathup)
import pickle
import numpy as np



import FilterStrategies.l2r_utils.h_l2rMiscellaneous as l2rMis
import FilterStrategies.l2r_utils.h_l2rMeasures as measures


import FilterStrategies.FilterFS.h_functionsFilter as hFilterFunctions
from FilterStrategies.l2r_utils.h_l2rMiscellaneous import getIdFeatureOrder
from collections import defaultdict

class basicStructure:
    def __init__(self):
        self.marginal = None
        self.mat = None
        self.pvalue = None
        self.variance = None
        self.greaterIsBetter = None

class dataset:
    def __init__(self):
        self.q = None
        self.x = None
        self.y = None


if __name__ == "__main__":

    predFile = str(sys.argv[1])
    corrFile = str(sys.argv[2])
    obj1 = str(sys.argv[3])
    obj2 = str(sys.argv[4])
    obj3 = str(sys.argv[5])
    statisticalDifference = float("0." + sys.argv[6])
    fold = int(sys.argv[7])
    coll = str(sys.argv[8])
    alpha = float(sys.argv[9])
    extraInfo = str(sys.argv[10])

    if "var" in obj1:
        print "ERROR: The objective 1 cannot receive the variance as a criteria. Because objective 1 only procced the statistical test with wilcoxon, not with F-Test"
        sys.exit(0)

    vetDominanceFeat = []
    prediction = basicStructure()
    risk = basicStructure()
    similarity = basicStructure()
    variance = basicStructure()
    print ("Processing FS " + coll + "  Fold" + str(fold) + "  " + obj1 + "." + obj2 + "." + obj3 + "." + extraInfo)

    #init = np.array([0.0500, 0.1500, 0.3000, 0.4500, 0.5500,0.4000, 0.3500, 0.3000, 0.2500, 0.2000,0.3000, 0.3000, 0.3000, 0.3000, 0.3000,0.2500, 0.2500, 0.2500, 0.2500, 0.2500,0.4000, 0.1500, 0.4000, 0.1500, 0.4000,0.2000,0.4500,0.2000, 0.4500, 0.2000,0.2542, 0.2629, 0.2802, 0.2975, 0.3061,0.2918, 0.2994, 0.3147, 0.3301, 0.3378])
    #init =np.reshape(init, (8,5))
    ######Defining Prediction data
    with open(predFile) as f:
        prediction.mat = pickle.load(f)
        #prediction.mat = init
    nFeatures = prediction.mat.shape[1]

    prediction.marginal = np.array([0.0] * nFeatures, dtype=float)
    for f in range(0, nFeatures):
        prediction.marginal[f] = np.mean(prediction.mat[:, f])

    prediction.pvalue=statisticalDifference
    prediction.greaterIsBetter = True
    prediction.variance = False
    ######Defining Distance data
    with open(corrFile) as f:
        similarity.mat = pickle.load(f)
        #similarity.mat = init

    similarity.marginal = np.array([0.0] * nFeatures, dtype=float)
    for f in range(0, nFeatures):
        similarity.marginal[f]=np.mean(similarity.mat[:,f])

    similarity.pvalue = statisticalDifference
    similarity.greaterIsBetter = False
    similarity.variance = False
    #####Defining Risk data
    risk = hFilterFunctions.gettingRiskData(obj1, obj2, obj3, extraInfo, prediction, alpha, nFeatures, statisticalDifference)
    #####Defining Variance data
    variance = hFilterFunctions.gettingVarianceData(obj1, obj2, obj3, extraInfo, prediction, coll, fold, nFeatures,
                                   statisticalDifference)
    ##########Finished the data loading
    ##############################################

    existPF = False
    paretoSet=[]

    setFeatures = set(range(nFeatures))
    featImportance = np.array([1000] * nFeatures, dtype=float)
    loopWeight=0
    while existPF == False:
        existPF=True

        vetDominanceFeat = hFilterFunctions.obtainDominace(obj1, obj2, obj3, prediction, similarity, risk, variance,  setFeatures, nFeatures)

        idF=0
        for f in vetDominanceFeat:
            if f==0 and idF not in paretoSet:
                existPF=False #### This means there are individuals who dominate, so it did not reach the convergency
                paretoSet.append( idF)
                featImportance[idF]=loopWeight
            idF=idF+1

        setFeatures = setFeatures - set(paretoSet)
        print "Part ", paretoSet
        if len(paretoSet) > nFeatures*0.4:
            existPF=True
        loopWeight+=1

    print "ParetoFrontier:", paretoSet
    #print featImportance
    if len(paretoSet) < nFeatures*0.4:
        print "!!!!!!!! IMPORTANT - The PF size reached a size smaller than ", 0.4, "of features!"

    nTrees = 300
    nRounds = 800

    test = dataset()
    train= dataset()
    testFile = "../Colecoes/" + coll + "/Fold" + str(fold) + "/Norm.test.txt"
    trainFile = "../Colecoes/" + coll + "/Fold" + str(fold) + "/Norm.train.txt"

    percTest = [0, 0.1, 0.2, 0.3, 0.4]
    for p in percTest:
        mask = getIdFeatureOrder(featImportance, p, nFeatures)
        #print mask
        individualFile = hFilterFunctions.getOutPuFile("convergency", coll, p, statisticalDifference, obj1, obj2, obj3, str(fold),
                                      predFile, corrFile, extraInfo);
        print "File:", individualFile

        with open(individualFile + ".ind", 'w') as f:
            f.write(mask)
    #test.x, test.y, test.q = l2rMis.load_L2R_file(testFile, mask)
    #train.x,train.y,train.q= l2rMis.load_L2R_file(trainFile, mask)

    #ndcg = np.array([0.0] * len(measures.getQueries(test.q)))
    #for exe in range(3):
    #    ndcg =  ndcg + l2rMis.getL2RPrediction ("1",  fold+exe, train, test, trainFile, testFile,  nTrees, mask, nFeatures )
    #ndcg = ndcg/3

    #with open(individualFile + ".rf.test.prediction", 'w') as f:
    #    for i in ndcg:
    #        f.write(str(i) + "\n")

    #ndcgLM = l2rMis.getL2RPrediction ("6",  fold, train, test, trainFile, testFile,  nRounds, mask, nFeatures)

    #with open(individualFile + ".lm.test.prediction", 'w') as f:
    #    for i in ndcgLM:
    #        f.write(i + "\n")



