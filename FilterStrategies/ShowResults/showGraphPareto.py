
from __future__ import division

import sys
import os.path
from subprocess import check_output
from collections import defaultdict
import re
from subprocess import call
from scipy.stats import wilcoxon
from scipy import stats

import plotly.plotly as py
import plotly.graph_objs as go

# Create random data with numpy
import numpy as np

fileName = str(sys.argv[1])

m = re.search('obj1(.*)\.obj2(.*)\.obj3(.*)\.', fileName)
if m:
    obj1 = m.group(1)
    obj2 = m.group(2)
    obj3 = m.group(3)

with open(fileName) as f:
    lines = f.readlines()

m = re.search('MaxMin'+obj1+'(.*) (.*)', lines[0])
if m:
    maxObj1 = float(m.group(1))
    minObj1 = float(m.group(2))

del lines[0]

m = re.search('MaxMin'+obj2+'(.*) (.*)', lines[0])
if m:
    maxObj2 = float(m.group(1))
    minObj2 = float(m.group(2))

del lines[0]
del lines[0]
print obj1, obj2, obj3
print "Max", maxObj1, maxObj2
print "Min", minObj1, minObj2

listID=[]
listObj1=[]
listObj2=[]
for i in lines:
    i=i.rstrip()
    partList=i.split(" ")
    listID.append(partList[0])
    listObj1.append(float(partList[1]))
    listObj2.append(float(partList[2]))

for i in range(len(listID)):
    print listID[i], listObj1[i], listObj2[i]

#N = 1000
#random_x = np.random.randn(N)
#random_y = np.random.randn(N)

# Create a trace
trace = go.Scatter(
    x=listObj1,
    y=listObj2,
    name = 'Individuas',
    mode='markers+text',
    text=listID,
    textposition='top center',
    marker=dict(
        size=10,
        color='rgba(152, 0, 0, .8)',
        line=dict(
            width=2,
            color='rgb(0, 0, 0)'
        )
    )
)
layout = go.Layout(
    xaxis=dict(
        title=obj1,
        range=[minObj1-0.01, maxObj1+0.01]
    ),
    yaxis=dict(
        title=obj2,
        range=[minObj2-0.01, maxObj2+0.01]
    )
)
data = [trace]

# Plot and embed in ipython notebook!
#py.iplot(data, filename='basic-scatter')
# or plot with: plot_url = py.plot(data, filename='basic-line')
fig= go.Figure(data=data, layout=layout)
#plot_url = py.plot(data,  layout=layout, filename='basic-line')
plot_url = py.plot(fig, filename='basic-line')