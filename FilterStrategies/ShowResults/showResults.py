from __future__ import division
__author__ = 'daniel'

import sys, os

from subprocess import call
pathup = os.path.abspath(os.path.join(
    os.path.dirname(__file__+"/../../"), os.path.pardir))
sys.path.insert(0, pathup)

#print(sys.path)
import FilterStrategies.l2r_utils.h_l2rMeasures as measures
import rpy2.robjects as robjects
import numpy as np
from FilterStrategies.FilterFS.h_functionsFilter import getOutPuFile


def getResult(listResult, riskBaseline, metric):
    if metric == "ndcg":
        return np.mean(np.asarray(listResult))

    if metric == "trisk":
        return  measures.getTRisk(np.asarray(listResult), riskBaseline, 5)

    if metric == "risk":
        return np.mean(measures.getRisk(listResult, riskBaseline))

def getResultBaseline(listResult, riskBaseline, metric, text):
    result=[]
    if metric == "ndcg":
        result = [text, "-", "-", np.mean(listResult), np.mean(listResult), np.mean(listResult),
                           np.mean(listResult), np.mean(listResult)]

    if metric == "trisk":
        result = [text, "-", "-", measures.getTRisk(listResult, riskBaseline, 5),
                           measures.getTRisk(listResult, riskBaseline, 5),
                           measures.getTRisk(listResult, riskBaseline, 5),
                           measures.getTRisk(listResult, riskBaseline, 5),
                           measures.getTRisk(listResult, riskBaseline, 5)]

    if metric == "risk":
        result =[text, "-", "-", np.mean(measures.getRisk(listResult, riskBaseline)), \
                    np.mean(measures.getRisk(listResult, riskBaseline)),\
                    np.mean(measures.getRisk(listResult, riskBaseline)),\
                    np.mean(measures.getRisk(listResult, riskBaseline)),\
                    np.mean(measures.getRisk(listResult, riskBaseline))]
    return result
def countFeatures(file):

    nfeatures=0
    #print "test", file
    with open(file) as f:
        for line in f:
            nfeatures+=line.count("1")
    #print nfeatures
    return nfeatures

def printLine (array):
    for i in array:
        print str(i)+" ",
    print " "

def getStatisticalTest(fullFeatures, lessFeatures, pvalue):

    rd1 = (robjects.FloatVector(fullFeatures))
    rd2 = (robjects.FloatVector(lessFeatures))
    rvtest = robjects.r['t.test']
    pvalueDiff = rvtest(rd1, rd2)[2][0]

    if pvalueDiff < pvalue:
        return "!"
    else:
        return "="

class basicStructure:
    def __init__(self):
        self.marginal = None
        self.mat = None
        self.pvalue = None


class dataset:
    def __init__(self):
        self.q = None
        self.x = None
        self.y = None

if __name__ == "__main__":

    coll = str(sys.argv[1])
    metric = str(sys.argv[2])
    algorithm = str(sys.argv[3])
    l2rResult = str(sys.argv[4])
    foldLimit = int(sys.argv[5])
    exp1 = str(sys.argv[6])

    experiments = []
    experiments.append(exp1.split(";"))
    if len(sys.argv)>7:
        exp2 = str(sys.argv[7])
        experiments.append(exp2.split(";"))
        if len(sys.argv[8]) > 8:
            exp3 = str(sys.argv[8])
            experiments.append(exp3.split(";"))
            if len(sys.argv[9])>9:
                exp4 = str(sys.argv[9])
                experiments.append(exp4.split(";"))
                if len(sys.argv[10])> 10:
                    exp5 = str(sys.argv[10])
                    experiments.append(exp5.split(";"))

    pvalues = [0.0, 0.01, 0.05, 0.1]
    featuresSizes = [0, 0.1, 0.2, 0.3, 0.4]
    #featuresSizes = [0]

    l2rMetric = "ndcg"

    ndcgBaseline = []
    fileName = "Dropbox/WorkingFiles/L2R_Baselines/"+ coll + "."+l2rResult + "."+l2rMetric+".test.Fold"
    print "FileBaseline", fileName
    for fold in range(1, foldLimit + 1):
        ndcgBaseline += list(np.loadtxt(fileName + str(fold)))

    singleRiskBaseline = []
    fileName = "Dropbox/WorkingFiles/L2R_Baselines/" + coll + ".max." + l2rMetric + ".test.Fold"
    for fold in range(1, foldLimit + 1):
        singleRiskBaseline += list(np.loadtxt(fileName + str(fold)))

    allResults =[]
    allResults.append(["method", "PFsize","statis", "pf", "0.1", "0.2", "0.3", "0.4"])

    allResults.append(getResultBaseline(ndcgBaseline, singleRiskBaseline, metric, "FULL"))


    fileName = "Dropbox/WorkingFiles/FilterStrategy/cikm/result."+coll+".bestPrediction.E_R.F"
    cikmResults = []
    for fold in range(1, foldLimit + 1):
        cikmResults += list(np.loadtxt(fileName+str(fold)+".rf.test.prediction"))

    allResults.append(getResultBaseline(cikmResults, singleRiskBaseline, metric, "CIKM"))

    for exp in experiments:

        for pv in pvalues:
            paretoSize =0

            for fold in range(1, foldLimit + 1):
                fileName = getOutPuFile(algorithm, coll, "0", pv, exp[0], exp[1], exp[2], fold, "'_ml1_", '_matPC_',exp[3])
                paretoSize += countFeatures("Dropbox/WorkingFiles/FilterStrategy/" + fileName + ".ind")

            txtDiff=""
            partResult = [""] * 8
            partResult[0]=algorithm+"."+exp[0]+"."+exp[1]+"."+exp[2]+"."+str(pv)
            #print paretoSize, foldLimit
            partResult[1]=paretoSize/foldLimit
            idr=3
            for perc in featuresSizes:

                listsPrecision = []
                for fold in range(1, foldLimit + 1):
                    fileName = getOutPuFile(algorithm, coll,perc, pv, exp[0], exp[1], exp[2], fold, "'_ml1_", '_matPC_', exp[3])
                    #print "\n"
                    #print "FILE: Dropbox/WorkingFiles/FilterStrategy/" + fileName + ".ind"

                    #fi = "Dropbox/WorkingFiles/FilterStrategy/" + fileName + ".ind"
                    #resp = call(["cat", fi],  stdout=True )
                    listsPrecision +=list(np.loadtxt("Dropbox/WorkingFiles/FilterStrategy/"+fileName+"."+l2rResult+".test.prediction"))
                    #print resp

                if len(ndcgBaseline) != len(listsPrecision):
                    print "Error!!! Different size of prediction file result", "base", len(ndcgBaseline), "result", len(listsPrecision)
                    sys.exit(0)

                if metric == "ndcg" or metric == "risk" or metric == "trisk":
                    txtDiff = getStatisticalTest(ndcgBaseline, listsPrecision, 0.05)

                partResult[2]=str(partResult[2])+txtDiff
                partResult[idr] = getResult(listsPrecision, singleRiskBaseline, metric)

                idr+=1
            allResults.append(partResult)


    for r in allResults:
        printLine(r)


