from __future__ import division
__author__ = 'daniel'

import sys, os
import os.path

pathup = os.path.abspath(os.path.join(
    os.path.dirname(__file__+"/../../"), os.path.pardir))
sys.path.insert(0, pathup)

#print(sys.path)
import time
import scipy.stats as stats
import pickle
import numpy as np
import re
from FilterStrategies.l2r_utils.h_l2rMiscellaneous import load_L2R_file
from FilterStrategies.l2r_utils.h_l2rMiscellaneous import getL2RPrediction
from FilterStrategies.l2r_utils.h_l2rMiscellaneous import getIdFeatureOrder

import FilterStrategies.l2r_utils.h_l2rMeasures as measures


from h_functionsFilter import getOutPuFile
from collections import defaultdict


class basicStructure:
    def __init__(self):
        self.marginal = None
        self.mat = None
        self.pvalue = None


class dataset:
    def __init__(self):
        self.q = None
        self.x = None
        self.y = None

if __name__ == "__main__":

    predFile = str(sys.argv[1])
    corrFile = str(sys.argv[2])
    obj1 = str(sys.argv[3])
    obj2 = str(sys.argv[4])
    obj3 = str(sys.argv[5])
    statisticalDifference = float("0."+sys.argv[6])
    fold = int(sys.argv[7])
    coll = str(sys.argv[8])
    alpha = float(sys.argv[9])
    extraInfo = str(sys.argv[10])


    if "var" in obj1:
        print "ERROR: The objective 1 cannot receive the variance as a criteria. Because objective 1 only procced the statistical test with wilcoxon, not with F-Test"
        sys.exit(0)

    vetDominanceFeat = []
    prediction = basicStructure()
    risk = basicStructure()
    similarity = basicStructure()
    variance = basicStructure()
    print ("Processing FS " + coll + "Fold"+ str(fold))



    #print "ParetoSize\tNDCG.RF\tTRisk.RF\tnqGain.RF\tNDCG.LM\tTRisk.LM\tnqGain.LM\tMask\n"

    nTrees = 300
    nRounds = 800

    test = dataset()
    train= dataset()
    testFile = "../Colecoes/" + coll + "/Fold" + str(fold) + "/Norm.test.txt"
    trainFile = "../Colecoes/" + coll + "/Fold" + str(fold) + "/Norm.train.txt"

    percTest = [ 0, 0.1, 0.2,0.3, 0.4]
    for p in percTest:


        individualFile=getOutPuFile("filter", coll, p, statisticalDifference, obj1, obj2, obj3, str(fold), predFile, corrFile, extraInfo);


        while os.path.isfile(individualFile+".ind") == False:
            print "Waiting file:", individualFile+".ind"
            time.sleep(60)

        with open(individualFile+".ind", 'r') as f:
            for line in f:
                mask = line

        print "Mask:", mask
        nFeatures = len(mask)
        test.x, test.y, test.q = load_L2R_file(testFile, mask)
        train.x,train.y,train.q= load_L2R_file(trainFile, mask)

        ndcg = np.array([0.0] * len(measures.getQueries(test.q)))


        for exe in range(3):
            ndcg = ndcg + getL2RPrediction("1", fold + exe, train, test, trainFile, testFile, nTrees, mask,  nFeatures)

        ndcg = ndcg / 3

        with open(individualFile + ".rf.test.prediction", 'w') as f:
            for i in ndcg:
                f.write(str(i) + "\n")

        #ndcgLM = np.array([0.0] * len(ndcg))
        #ndcgLM = getL2RPrediction("6", fold, train, test, trainFile, testFile, nRounds, mask, nFeatures)
        #with open(individualFile + ".lm.test.prediction", 'w') as f:
        #    for i in ndcgLM:
        #        f.write(str(i) + "\n")
            ##print "Size("+str(p)+")",str(mask.count("1")), ndcgRF, triskRF, nqGainRF, ndcgLM, triskLM, nqGainLM, mask

