from __future__ import division
__author__ = 'daniel'

import sys
import scipy.stats as stats
import pickle
import numpy as np
import re
from h_l2rMiscellaneous import load_L2R_file
from h_l2rMiscellaneous import getL2RPrediction
from h_l2rMiscellaneous import getIdFeatureOrder

from h_l2rMeasures import getQueries
from h_l2rMeasures import modelEvaluation
from h_l2rMeasures import modelEvaluationScript
from h_l2rMeasures import modelRiskEvaluation

from h_functionsFilter import dominate
from collections import defaultdict

coll = str(sys.argv[1])
scoreFile = str(sys.argv[2])
fold  = str(sys.argv[3])


testFile = "/home/daniel/Colecoes/" + coll + "/Fold" + str(fold) + "/Norm.test.txt"
scoreList = np.loadtxt(scoreFile)
ndcgScript= modelEvaluationScript(scoreList, testFile, "NDCG")

X_test, y_test, query_id_test = load_L2R_file(testFile)
ndcg, map = modelEvaluation(y_test, scoreList, query_id_test, 136)

i=0
while i< len(ndcgScript):
    if ndcg[i]<>ndcgScript[i]:
        print str(i)+": "+str(ndcg[i])+" "+str(ndcgScript[i])
    i=i+1

print np.mean(ndcgScript)

print np.mean(ndcg)


#testFile = "/home/daniel/Colecoes/" + coll + "/Fold" + str(fold) + "/Norm.test.txt"
#X_test, y_test, query_id_test = load_L2R_file(testFile)
#queriesList = getQueries(query_id_test)

#queryProb=[447,470,509,736,965,1094,1232,1499,1511,1647,1659,1777,1973]

#scoreList = np.loadtxt(scoreFile)

#testOriginal = []
#with open (testFile, 'r') as fTest:
#    for line in fTest:
#        testOriginal.append(line)

#with open('scoreQP', 'w') as f:
#    for q in queryProb:
#        qIdx = np.asarray(queriesList[q])
#        for s in qIdx:
#            f.write(str(scoreList[s]) + "\n")

#with open('testQP', 'w') as f:
#    for q in queryProb:
#        qIdx = np.asarray(queriesList[q])
#        for s in qIdx :
#            f.write(testOriginal[s])


#print "Fim"
