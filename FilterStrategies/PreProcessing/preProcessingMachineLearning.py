from __future__ import division
__author__ = 'daniel'

import scipy.stats as stats
import pickle
import numpy as np
import re
import Utils_L2R.h_l2rMiscellaneous as l2r
import Utils_L2R.h_l2rMeasures as l2rMeasures
import sys, os

pathup = os.path.abspath(os.path.join(
    os.path.dirname(__file__+"/../../"), os.path.pardir))
sys.path.insert(0, pathup)





from sklearn import model_selection
from FilterStrategies.FilterFS.h_functionsFilter import buildMatrixPredCorrelation
from sklearn import linear_model
from sklearn.ensemble import RandomForestRegressor


class Dataset:
    def __init__(self):
        self.q = None
        self.x = None
        self.y = None


def ml_executeSckitLearn (train, test, seed, nTrees, l2r=1):

    seed = seed + 10
    clf = None

    if (l2r==1):
        clf = RandomForestRegressor(n_estimators=nTrees, max_features="auto", min_samples_leaf=1, random_state=seed, n_jobs=-1)
    elif (l2r==4):
        clf = linear_model.LinearRegression()


    clf.fit((train.x).reshape(-1,1) , train.y)
    resScore = clf.predict((test.x).reshape(-1,1))

    return resScore


def ml_processFeature(yData, xData, qData, queriesList, fold, nFeature, l2r):

    prediction = [0.0] * len(queriesList)

    ###### Making CROSSVALIDATION

    kf = model_selection.KFold(n_splits=3, shuffle=True, random_state=fold+10)
    XF_train_index = []
    XF_test_index = []

    for qtrain_index, qtest_index in kf.split(queriesList):
        # print("QTEST:", qtest_index)
        # print ("size:", len(qtest_index))
        # print("QTRAIN:", qtrain_index)
        # print ("size:", len(qtrain_index))
        del XF_train_index[:]
        del XF_test_index[:]

        for qtr in qtrain_index:
            XF_train_index = XF_train_index + queriesList[qtr]
        for qts in qtest_index:
            XF_test_index = XF_test_index + queriesList[qts]

        test = Dataset()
        train = Dataset()
        train.x, test.x = xData[XF_train_index], xData[XF_test_index]
        train.y, test.y = yData[XF_train_index], yData[XF_test_index]
        train.q, test.q = qData[XF_train_index], qData[XF_test_index]

        resScore = None;
        if (l2r == 6):
            resScore = l2r.executeLambdaMART(train, test,  fold, "NDCG", 100)
        else:
            resScore = ml_executeSckitLearn(train, test, fold, 100, l2r)

        ndcg, map = l2rMeasures.modelEvaluation(test, resScore, nFeature)

        i=0
        for q in qtest_index:
            prediction[q] = ndcg[i]
            i = i + 1

    return prediction



def ml_buildMatrixPrediction(train, fold, metric="NDCG", l2rAlgorithm=1):
    queriesList = l2rMeasures.getQueries(train.q)
    nFeatures = train.x.shape[1]
    nQueries = len(queriesList)
    matPrediction = np.ndarray(shape=(nQueries, nFeatures), dtype=float, order='F')

    for f in range(0, nFeatures):
        prediction = ml_processFeature(train.y, train.x[: , f], train.q, queriesList, fold, nFeatures, l2rAlgorithm)
        matPrediction[:,f]=prediction

    return matPrediction



TRAIN_FILE_NAME = "C:\Users\adilioalvxs\Documents\Instituto\Pesquisa\Codigo\2003_td_dataset\Fold1\Norm.train.txt"
fold = 1
COLL = "2003_td_dataset"
L2R = 1
print TRAIN_FILE_NAME, L2R

if "web10k" in TRAIN_FILE_NAME:
    mask="1" * 136
elif "2003_td_dataset" in TRAIN_FILE_NAME:
    mask = "1" * 64


train = Dataset()
train.x, train.y, train.q = l2r.load_L2R_file(TRAIN_FILE_NAME, mask)

####PREDICTION
matPredFeature=ml_buildMatrixPrediction(train, fold, "NDCG", L2R )

####RiskSensitiveness
#riskBaseline = buildSingleRiskBaseline (matPredFeature)
#matSingleRiskFeature=buildMatrixSingleRisk(matPredFeature, riskBaseline)
#vetSingleTriskFeature=buildMatrixSingleTrisk(matPredFeature, riskBaseline)

####Correlation
#matKC_NaiveFeature = buildMatrixNaiveCorrelation(X_train, query_id_train, "kendalltau")
#matPC_NaiveFeature = buildMatrixNaiveCorrelation(X_train, query_id_train ,  "pearson")
matKC_PredFeature = buildMatrixPredCorrelation(matPredFeature, "kendalltau")
matPC_PredFeature = buildMatrixPredCorrelation(matPredFeature, "pearson")

#print matKC_NaiveFeature
#print matPC_NaiveFeature
#print matKC_PredFeature
#print matPC_PredFeature


with open(COLL+'_ml'+str(L2R)+'_matPredFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matPredFeature, f)
#with open('/home/daniel/'+COLL+'matSingleRiskFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
#    pickle.dump(matSingleRiskFeature, f)
#with open('/home/daniel/'+COLL+'vetSingleTriskFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
#    pickle.dump(vetSingleTriskFeature, f)
#with open('/home/daniel/'+COLL+'matKC_NaiveFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
#    pickle.dump(matKC_NaiveFeature, f)
#with open('/home/daniel/'+COLL+'matPC_NaiveFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
#    pickle.dump(matPC_NaiveFeature, f)


with open(COLL+'_ml'+str(L2R)+'_matKC_PredFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matKC_PredFeature, f)
with open(COLL+'_ml'+str(L2R)+'_matPC_PredFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matPC_PredFeature, f)
