from __future__ import division
__author__ = 'daniel'

import sys
import scipy.stats as stats
import pickle
import numpy as np
import re
from h_l2rMiscellaneous import load_L2R_file
from h_l2rMeasures import getQueries
from h_l2rMeasures import modelEvaluation
from h_l2rMeasures import modelRiskEvaluation
from h_functionsFilter import buildMatrixNaiveCorrelation
from h_functionsFilter import buildMatrixPredCorrelation


def getRiskArray(aps, base):
    vetDelta= []
    i=0;
    for my in aps:
        deltaRisk= base[i]-my;
        i=i+1;
        if(deltaRisk < 0):
            vetDelta.append(0)
        else:
            vetDelta.append(deltaRisk)
    return np.asarray(vetDelta)

def buildSingleRiskBaseline(mat, metric="mean"):
    baseline = np.array([0.0]*mat.shape[0], dtype=float)

    for q in range(0, mat.shape[0]):
        if (metric == "mean"):
            baseline[q]= np.average(mat[q,:])

    return baseline

def buildMatrixSingleTrisk(matPredFeature, riskBaseline):
    nFeatures = matPredFeature.shape[1]
    vet = np.array([0.0]*nFeatures, dtype=float)
    for f in range(0, nFeatures):
        vet[f]=modelRiskEvaluation(matPredFeature[:,f], riskBaseline, metric="trisk")

    return vet

def buildMatrixSingleRisk(matPredFeature, riskBaseline):
    nFeatures = matPredFeature.shape[1]
    nQueries = matPredFeature.shape[0]
    mat = np.zeros(shape=(nQueries, nFeatures))

    for f in range(0, nFeatures):

        mat[:,f]=getRiskArray(matPredFeature[:,f], riskBaseline)

    return mat






def buildMatrixPrediction(x_train, y_train, ql_train, metric="NDCG"):
    queriesList = getQueries(query_id_train)
    nFeatures = len(X_train[0])
    nQueries = len(queriesList)
    matPrediction = np.ndarray(shape=(nQueries, nFeatures), dtype=float, order='F')

    for q in range (0, nQueries):
        qIdx = np.asarray(queriesList[q])

        for f in range(0, nFeatures):
            qNDCG, qMAP= modelEvaluation(y_train[qIdx[:, None]], x_train[qIdx[:, None], f], ql_train[qIdx[:, None]], nFeatures)

            if (metric == "NDCG"):
                matPrediction[q][f]=qNDCG
            else:
                matPrediction[q][f] = qMAP

    return matPrediction



TRAIN_FILE_NAME = str(sys.argv[1])
fold = str(sys.argv[2])
COLL = str(sys.argv[3])
print TRAIN_FILE_NAME
X_train, y_train, query_id_train = load_L2R_file(TRAIN_FILE_NAME)

####PREDICTION
matPredFeature=buildMatrixPrediction(X_train, y_train,query_id_train, "NDCG")

####RiskSensitiveness
riskBaseline = buildSingleRiskBaseline (matPredFeature)
matSingleRiskFeature=buildMatrixSingleRisk(matPredFeature, riskBaseline)
vetSingleTriskFeature=buildMatrixSingleTrisk(matPredFeature, riskBaseline)

####Correlation
matKC_NaiveFeature = buildMatrixNaiveCorrelation(X_train, query_id_train, "kendalltau")
matPC_NaiveFeature = buildMatrixNaiveCorrelation(X_train, query_id_train ,  "pearson")
matKC_PredFeature = buildMatrixPredCorrelation(matPredFeature, "kendalltau")
matPC_PredFeature = buildMatrixPredCorrelation(matPredFeature, "pearson")

#print matKC_NaiveFeature
#print matPC_NaiveFeature
#print matKC_PredFeature
#print matPC_PredFeature


with open('/home/daniel/'+COLL+'matPredFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matPredFeature, f)
with open('/home/daniel/'+COLL+'matSingleRiskFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matSingleRiskFeature, f)
with open('/home/daniel/'+COLL+'vetSingleTriskFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(vetSingleTriskFeature, f)
with open('/home/daniel/'+COLL+'matKC_NaiveFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matKC_NaiveFeature, f)
with open('/home/daniel/'+COLL+'matPC_NaiveFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matPC_NaiveFeature, f)
with open('/home/daniel/'+COLL+'matKC_PredFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matKC_PredFeature, f)
with open('/home/daniel/'+COLL+'matPC_PredFeature.NDCG10.Fold'+str(fold)+'.pickle', 'w') as f:
    pickle.dump(matPC_PredFeature, f)
